# README #

Code for the all lambdas + swagger/API Gateway yaml.

The tasklist application:


A DynamoDB database was created.
Four Lambda functions were written for processing API Gateway input and making CRUD operations on dynamodb.
An IAM lambda execution role was created and assigned to each lambda.
Swagger API definition was imported into API Gateway to create api resources.
API Gateway paths were mapped via lambda integration to the respective lambda functions.
API Gateway api deployed to a stage. [https://1srk6yod7d.execute-api.us-east-1.amazonaws.com/dev/tasks/](https://1srk6yod7d.execute-api.us-east-1.amazonaws.com/dev/tasks/)

Another lambda function 'ReminderBot' was written to daily scan the dynamodb and email all users with incomplete tasks.
The lambda creates an email message listing all the users incomplete tasks and sends it using Amazon SES.
A CloudWatch event was created to trigger the ReminderBot lambda every 24 hours.

api:

 /
 /tasks
GET
POST
 /{taskid}
DELETE
GET
PUT

Note:
The dynamo db name is currently hard-coded in the lambda functions : “tasklist4”
It is assumed that only completed tasks have value in the 'completed' attribute.

* Arun Manta