package com.arun;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class AddTask implements RequestHandler<Map<String,Object>, Object> {

    @Override
    public Object handleRequest(Map<String,Object> input, Context context) {
    	Table table = Util.getTable();
        
        try {
            context.getLogger().log("Adding a new item...");
            PutItemOutcome outcome = table.putItem(Item.fromMap(input));
            context.getLogger().log("PutItem succeeded:\n" + outcome.getPutItemResult());

        } catch (Exception e) {
        	context.getLogger().log("Unable to add item: " + input + " ");
        	context.getLogger().log(e.getMessage());
        }
        
        return "200";
    }

}
