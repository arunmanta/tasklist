package com.arun;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.DeleteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.KeyAttribute;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class DeleteTask implements RequestHandler<Map<String,Object>, Object> {

    @Override
    public Object handleRequest(Map<String,Object> input, Context context) {
    	Table table = Util.getTable();
        
        try {
            context.getLogger().log("Deleting an item..." + input);
            DeleteItemSpec deleteItemSpec = new DeleteItemSpec().withPrimaryKey(new KeyAttribute("taskid",input.get("taskid")));
            DeleteItemOutcome outcome = table.deleteItem(deleteItemSpec);
            context.getLogger().log("Item deleted successfully:\n" + outcome.getDeleteItemResult());

        } catch (Exception e) {
        	context.getLogger().log("Unable to delete item: " + input + " ");
        	context.getLogger().log(e.getMessage());
        }
        
        return "200";
    }

}
