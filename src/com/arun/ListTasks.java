package com.arun;

import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.KeyAttribute;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class ListTasks implements RequestHandler<Map<String,Object>, Object> {

    @Override
    public Object handleRequest(Map<String,Object> input, Context context) {
    	Table table = Util.getTable();
        
        int size = input.containsKey("size")?(int)input.get("size"):10;
        if(input.containsKey("taskid")){
        	Item item = table.getItem(new KeyAttribute("taskid", input.get("taskid")));
        	if(item != null)
        		return item.toJSONPretty();
        	else
        		return "";
        }
        
	    ItemCollection<ScanOutcome> items = table.scan(new ScanSpec().withMaxResultSize(size));
	    
        StringBuilder sb = new StringBuilder();       
        for(Item item : items){
        	context.getLogger().log(item.toJSONPretty());
            sb.append(item.toJSON());
        }
        return sb.toString();
    }

}
