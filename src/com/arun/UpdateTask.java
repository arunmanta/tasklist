package com.arun;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.amazonaws.services.dynamodbv2.document.AttributeUpdate;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class UpdateTask implements RequestHandler<Map<String,Object>, Object> {

    @Override
    public Object handleRequest(Map<String,Object> input, Context context) {
    	Table table = Util.getTable();
        
        Integer taskid = (Integer)input.get("taskid");
        if(taskid == null)
        	return "404";
        
        context.getLogger().log("Updating an item..." + input);
        
        Map<String,Object> payload = (Map<String, Object>)input.get("body");
        List<AttributeUpdate> updates = new ArrayList<>();
        for(Entry<String, Object> entry: payload.entrySet()){
        	updates.add(new AttributeUpdate(entry.getKey()).put(entry.getValue()));
        }
                        
        try {
        	UpdateItemOutcome outcome = table.updateItem(
                    new UpdateItemSpec()
                    .withPrimaryKey("taskid", input.get("taskid"))
                    .withAttributeUpdate(updates).withReturnValues(ReturnValue.UPDATED_NEW));
        	
            context.getLogger().log("UpdateItem succeeded:\n" + outcome.getUpdateItemResult());

        } catch (Exception e) {
        	context.getLogger().log("Unable to update item: " + input + " ");
        	context.getLogger().log(e.getMessage());
        }
        
        return "200";
    }

}
