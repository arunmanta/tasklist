package com.arun;

import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.simpleemail.*;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;

public class Util {

	static Table getTable() {
		AmazonDynamoDBClient client = new AmazonDynamoDBClient();
	    client.withRegion(Regions.US_EAST_1);
	    DynamoDB db = new DynamoDB(client);
	    Table table = db.getTable("tasklist4");
		return table;
	}
	
	
	static void sendEmail(String address, String subject, String body){		
		String from = "task.reminder@example.com";
		Destination destination = new Destination().withToAddresses(address);
		Content _subject = new Content().withData(subject);
        Content textBody = new Content().withData(body); 
        Body _body = new Body().withText(textBody);
        
        Message message = new Message().withSubject(_subject).withBody(_body);
        
        SendEmailRequest request = new SendEmailRequest().withSource(from).withDestination(destination).withMessage(message);
        
        try
        {        
            System.out.println("Attempting to send an email");
        
            AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient();
               
            Region REGION = Region.getRegion(Regions.US_EAST_1);
            client.setRegion(REGION);
       
            // Send the email.
            client.sendEmail(request);  
            System.out.println("Email sent!");
        }
        catch (Exception ex) 
        {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
        }
	}

}
