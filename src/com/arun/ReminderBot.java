package com.arun;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.ScanFilter;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class ReminderBot implements RequestHandler<Map<String,Object>, Object>{

	@Override
	public Object handleRequest(Map<String, Object> input, Context context) {
		Table table = Util.getTable();
		
		StringBuilder sb = new StringBuilder();
		try{
			ItemCollection<ScanOutcome> items = table.scan(new ScanSpec().withScanFilters(new ScanFilter("completed").notExist()));
			
			Map<String, List<Item>> emails = new HashMap<>(); 
			Iterator<Item> iterator = items.iterator();
			Item item = null;
			while (iterator.hasNext()) {
				item = iterator.next();
				String user = (String)item.get("user");
				if(user != null){
					List<Item> tasks = emails.getOrDefault(user, new ArrayList<>());
					tasks.add(item);
					emails.put(user, tasks);
				}
				
				sb.append(item.toJSONPretty());
			    context.getLogger().log(item.toJSONPretty());
			    
			}
			sendEmails(context, emails);
			
		}catch(Exception e){
			context.getLogger().log("could not get items" + e);
		}
		
		return sb.toString();
	}

	private static void sendEmails(Context context, Map<String, List<Item>> emails) {
		emails.forEach((address,tasks)->{
			StringBuilder email = new StringBuilder();
			email.append("Dear ");
			email.append(address);
			email.append("\nYou have the following unfinished tasks");
			tasks.forEach((item)->{
				email.append("\n*  "+item.get("description"));
				});
			sendEmail(context, address, email.toString());
			});
	}

	private static void sendEmail(Context context, String address, String email) {
		context.getLogger().log("Sending email to "+ address);
		context.getLogger().log("email: ");
		context.getLogger().log(email);
		Util.sendEmail(address, "You have tasks!", email);
	}

}
